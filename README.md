# Oracle Database Auto Backup Script

/**
 * @author Jaber
 * @date 29/11/2022
 * @time 08:05 PM
 */

Syntax: 

	[expdp username/password@SidOrServiceName schemas=SchemaToExport directory=exportDirectory dumpfile=fileNaming.DMP logfile=logNaming.log]


Example:

	[expdp jaber/12345@orcl schemas=STUDENT_SCHEMA directory=ORACLECLRDIR dumpfile=STUDENT_SCHEMA_%fullstamp%_.DMP logfile=STUDENT_SCHEMA_%fullstamp%_log.log]


<!-- ===========================> File naming based on the current date and time <===================== -->

	@echo off
	for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
	set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
	set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
	set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%" & set "fullstamp=%YYYY%-%MM%-%DD%_%HH%%Min%-%Sec%"

	expdp jaber/12345@orcl schemas=ERP directory=ORACLECLRDIR dumpfile=STUDENT_SCHEMA_%fullstamp%_.DMP logfile=STUDENT_SCHEMA_%fullstamp%_log.log


	[Note: ORACLECLRDIR is a default directory provided by oracle.
	Path: D:\Jaber\Oracle19c\db_home\bin\clr]


# Steps:
	1. Save this file with .bat extension. [backup.bat]
	2. Search Task Scheduler on your windows operating system
	3. Click on Create Task
	4. Give a name of your scheduler under general tab.
	5. Click on trigger tab and select a corresponding date and time.
		[based on that given time the task schedular will execute the task]
	6. From the action tab click -> new and then browse the .bat file 
		[My bat file location: C:\Users\hp\Desktop\import.bat]
	7. Click Ok
	
	Now you're done. This scheduler will execute by your given schedule.
	